![WARNING](warning.png)

# krlist

`krlist.yaml` is a ruleset of [Clash](https://github.com/Dreamacro/clash), contains domains that may trigger redirection to http://warning.or.kr/, or other restrictions by KCSC or the site itself.

# Howtouse
Below is an example for your `config.yaml`
```
rule-providers:
  krlist:
    type: http
    behavior: classical
    path: ./Rules/krlist
    url: 'https://ygsk10.gitlab.io/krlist/krlist.yaml'
    interval: 86400

rules:
- RULE-SET,krlist,nokr
```
# See Also
[한국에서 인터넷 검열을 피하는 방법](https://github.com/preco21/bypass-censorship-korea)  

[유해 사이트/목록-나무위키](https://namu.wiki/w/유해%20사이트/목록)

[github.com/matthewruttley/contentfilter](https://github.com/matthewruttley/contentfilter)

[How India Censors The Web -- Data](https://github.com/kush789/How-India-Censors-The-Web-Data)

[Kushagra Singh*, Gurshabad Grover*, and Varun Bansal.
2020. How India Censors the Web.
In 12th ACM Conference on Web Science (WebSci ’20), July 6–10, 2020, Southampton, United Kingdom. ](https://doi.org/10.1145/3394231.3397891) 
